//
//  main.m
//  kr2utf8
//
//  Created by Kwanghoon Choi on 2014. 4. 20..
//  Copyright (c) 2014년 Kwanghoon Choi. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    @autoreleasepool {
        NSString *filepath = [NSString stringWithCString:argv[1]
                                                encoding:NSUTF8StringEncoding];
        if ([filepath hasPrefix:@"/"] || [filepath hasPrefix:@"~/"]) {
            filepath = [[NSFileManager defaultManager].currentDirectoryPath stringByAppendingPathComponent:filepath];
        }
        
        NSLog(@"source file -> %@", filepath);
        NSError *err = nil;
        NSString *text = [NSString stringWithContentsOfFile:filepath
                                                   encoding:0x80000000 + kCFStringEncodingDOSKorean
                                                      error:&err];
        if( nil == err) {
            [text writeToFile:filepath
                   atomically:YES
                     encoding:NSUTF8StringEncoding
                        error:nil];
        }
        else {
            NSLog(@"Don't need convert to utf-8");
        }
        // 0x80000000 + kCFStringEncodingDOSKorean
    }
    return 0;
}

